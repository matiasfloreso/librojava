public class Principal {

    public static void main(String [] args){
        Libro libro1 = new Libro();
        Libro libro2 = new Libro();

        libro1.setIsbn(111111111);
        libro1.setAutor("Frank Kafka");
        libro1.setTitular("Metamorfosis");
        libro1.setNumeroPaginas(70);

        libro2.setIsbn(222222222);
        libro2.setAutor("Jose Hernandez");
        libro2.setTitular("Martin Fierro");
        libro2.setNumeroPaginas(70);

        System.out.println(libro1);
        System.out.println(libro2);
    }
    
}
