public class Libro{
    private int isbn;
    private String titulo;
    private String autor;
    private int numeroPaginas;

    public void setIsbn(int isbn){
        this.isbn = isbn;
    }

    public int getIsbn(){
        return this.isbn;
    }

    public void setTitular(String titulo){
        this.titulo = titulo;
    }

    public String getTitular(){
        return titulo;
    }

    public void setAutor(String autor){
        this.autor = autor;
    }

    public String getAutor(){
        return autor;
    }

    public void setNumeroPaginas(int numeroPaginas){
        this.numeroPaginas = numeroPaginas;
    }

    public int getNumeroPaginas(){
        return numeroPaginas;
    }

    public String toString(){
        return "El libro ISBN : "+ isbn +", creado por el autor: "+ autor +", tiene "+ numeroPaginas +" paginas"; 
    }

    
}